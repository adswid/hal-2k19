#ifndef __adc_H
#define __adc_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f1xx_hal.h"
#include "main.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;

extern void _Error_Handler(char *, int);

void MX_ADC1_Init(void);
void MX_ADC2_Init(void);

volatile uint16_t adcValue[6];
volatile uint16_t current[2];

void ADC1_2_IRQHandler(void);

#ifdef __cplusplus
}
#endif
#endif /*__ adc_H */
